package pl.edu.agh.iwium.battle;

public class Reward {

    public static final int IS_ALIVE = 3;

    public static final int BULLET_HIT = 60;

    public static final int HIT_WALL = -10;

    public static final int HIT_BY_BULLET = -20;

    public static final int BULLET_MISSED = -1;


}
