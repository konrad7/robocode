package pl.edu.agh.iwium.battle;

public class Game {

    private static final int DISCRETIZATION_Y = 15;
    private static final int DISCRETIZATION_X = 15;
    private static final int DISCRETIZATION_ANGLE = 10;

    private double battlefieldHeight;
    private double battlefieldWidth;
    private int enemyBearing;


    public Game(double battleFieldHeight, double battleFieldWidth) {
        this.battlefieldHeight = battleFieldHeight;
        this.battlefieldWidth = battleFieldWidth;
    }

    public int discretizeY(double value) {
        return discretize(value, battlefieldHeight, DISCRETIZATION_Y);
    }

    public int discretizeX(double value) {
        return discretize(value, battlefieldWidth, DISCRETIZATION_X);
    }

    public int discretizeHeading(double value) {
        return discretize(value, 360, DISCRETIZATION_ANGLE);
    }

    public int discretizeBearing(double value) {
        double newVal = value + 180.0;
        return discretize(newVal, 360, DISCRETIZATION_ANGLE);
    }

    private int discretize(double value, double maxValue, int maxIntervals) {
        double interval = maxValue / (double) maxIntervals;

        for (int i = 1; i < maxIntervals; i++) {
            double tmp = i * interval;
            if (value < tmp) {
                return i;
            }
        }
        return maxIntervals;
    }

    public int getEnemyBearing() {
        return enemyBearing;
    }

    public void setEnemyBearing(double enemyBearing) {
        this.enemyBearing = discretizeBearing(enemyBearing);
    }
}
