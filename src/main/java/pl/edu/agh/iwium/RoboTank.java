package pl.edu.agh.iwium;

import pl.edu.agh.iwium.action.Action;
import pl.edu.agh.iwium.battle.Game;
import pl.edu.agh.iwium.battle.Reward;
import pl.edu.agh.iwium.learn.Observation;
import pl.edu.agh.iwium.learn.QLearner;
import robocode.*;

import java.util.ArrayList;
import java.util.List;

public class RoboTank extends AdvancedRobot {

    private int totalReward = 0;
    private int reward = 0;
    private QLearner qLearner;
    private List<Integer> totalRewards = new ArrayList<>();
    private RobotStatus robotStatus;
    private Game game;

    public void run() {
        game = new Game(getBattleFieldHeight(), getBattleFieldWidth());
        qLearner = new QLearner(getDataFile("model.ser"));

        while (true) {
            reward = isAlive() ? Reward.IS_ALIVE : 0;

            Observation observation = getObservation();
            Action action = qLearner.getAction(observation);
            action.execute(this);
            Observation newObservation = getObservation();
            qLearner.update(observation, newObservation, action, reward);
            totalReward += reward;
        }
    }

    private Observation getObservation() {
        int x = game.discretizeX(robotStatus.getX());
        int y = game.discretizeY(robotStatus.getY());
        int heading = game.discretizeHeading(robotStatus.getHeading());
        return new Observation(x, y, heading, game.getEnemyBearing());
    }

    @Override
    public void onStatus(StatusEvent event) {
        robotStatus = event.getStatus();
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
        game.setEnemyBearing(event.getBearing());
    }

    @Override
    public void onHitWall(HitWallEvent event) {
        reward += Reward.HIT_WALL;
    }

    @Override
    public void onHitByBullet(HitByBulletEvent event) {
        reward += Reward.HIT_BY_BULLET;
    }

    @Override
    public void onBulletHit(BulletHitEvent event) {
        reward += Reward.BULLET_HIT;
    }

    @Override
    public void onBulletMissed(BulletMissedEvent event) {
        reward += Reward.BULLET_MISSED;
    }

    @Override
    public void onRoundEnded(RoundEndedEvent event) {
        qLearner.saveModel();
        totalRewards.add(totalReward);
        reward = 0;
    }

    @Override
    public void onBattleEnded(BattleEndedEvent event) {
        qLearner.saveModel();
    }

    private boolean isAlive() {
        return robotStatus.getEnergy() > 0.0;
    }
}
