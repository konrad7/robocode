package pl.edu.agh.iwium.learn;

import java.io.Serializable;

public class Observation implements Serializable {

    private int x;
    private int y;
    private int heading;
    private int enemyBearing;

    public Observation() {

    }

    public Observation(int x, int y, int heading, int enemyBearing) {
        this.x = x;
        this.y = y;
        this.heading = heading;
        this.enemyBearing = enemyBearing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Observation that = (Observation) o;

        if (x != that.x) return false;
        if (y != that.y) return false;
        if (heading != that.heading) return false;
        return enemyBearing == that.enemyBearing;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + heading;
        result = 31 * result + enemyBearing;
        return result;
    }
}
