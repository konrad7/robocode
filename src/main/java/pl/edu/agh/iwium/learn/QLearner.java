package pl.edu.agh.iwium.learn;

import pl.edu.agh.iwium.action.Action;
import pl.edu.agh.iwium.action.ActionManager;
import robocode.RobocodeFileOutputStream;

import java.io.*;
import java.util.*;

public class QLearner {

    private static final double ALPHA = 0.7;
    private static final double EPSILON = 0.4;
    private static final double GAMMA = 0.8;

    private Random random = new Random();
    private ActionManager actionManager = new ActionManager();
    private Map<Observation, Map<Action, Double>> model = new HashMap<>();
    private File modelFile;

    public QLearner(File file) {
        modelFile = file;
        if (modelFile.isFile()) {
            try {
                FileInputStream fos = new FileInputStream(modelFile);
                ObjectInputStream oos = new ObjectInputStream(fos);
                model = (HashMap<Observation, Map<Action, Double>>) oos.readObject();
                oos.close();
                fos.close();
                System.out.printf("Model loaded");
            } catch (IOException | ClassNotFoundException e) {
                model = new HashMap<>();
            }
        }
    }

    public Action getAction(Observation state) {
        if (random.nextDouble() <= EPSILON) {
            return actionManager.getRandomAction();
        }
        Action bestAction = getBestAction(state);
        return bestAction != null ? bestAction : actionManager.getRandomAction();
    }

    public void update(Observation before, Observation after, Action action, int reward) {
        double newQValue = (1 - ALPHA) * getQ(before, action) + ALPHA * (reward + GAMMA * getMaxQ(after));
        getObservation(before).put(action, newQValue);
    }

    private Map<Action, Double> getObservation(Observation observation) {
        if (!model.containsKey(observation)) {
            Map<Action, Double> actionValues = new HashMap<>();
            actionManager.getActions().forEach(action -> actionValues.put(action, 0.0));
            model.put(observation, actionValues);
        }
        return model.get(observation);
    }

    private double getQ(Observation observation, Action action) {
        return Optional.ofNullable(model.get(observation))
                .map(actionValue -> actionValue.get(action))
                .orElse(0.0);
    }

    private double getMaxQ(Observation observation) {
        return Optional.ofNullable(model.get(observation))
                .orElse(Collections.emptyMap())
                .values()
                .stream()
                .max(Double::compareTo)
                .orElse(0.0);
    }

    private Action getBestAction(Observation observation) {
        return Optional.ofNullable(model.get(observation))
                .orElse(Collections.emptyMap())
                .entrySet()
                .stream()
                .sorted(Map.Entry.<Action, Double>comparingByValue().reversed())
                .findFirst()
                .map(Map.Entry::getKey)
                .orElse(null);
    }

    public void saveModel() {
        try (RobocodeFileOutputStream stream = new RobocodeFileOutputStream(modelFile)) {
            ObjectOutputStream oos = new ObjectOutputStream(stream);
            oos.writeObject(model);
            oos.close();
            System.out.printf("Model saved");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
