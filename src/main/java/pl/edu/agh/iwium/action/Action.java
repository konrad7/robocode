package pl.edu.agh.iwium.action;

import robocode.AdvancedRobot;

import java.io.Serializable;

public interface Action extends Serializable {

    double MOVE_DISTANCE = 80.0;
    double TURN_DEGREES = 30.0;
    double FIRE_POWER = 1.0;

    void execute(AdvancedRobot robot);

}
