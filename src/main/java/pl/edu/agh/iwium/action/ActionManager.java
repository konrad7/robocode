package pl.edu.agh.iwium.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ActionManager {

    private final List<Action> actions = new ArrayList<>();
    private final Random random = new Random();
    private final int numberOfActions;

    public ActionManager() {
        this.actions.add(new Ahead());
        this.actions.add(new Back());
        this.actions.add(new TurnLeft());
        this.actions.add(new TurnRight());
        this.actions.add(new Fire());
        this.numberOfActions = actions.size();
    }

    public Action getRandomAction() {
        return actions.get(random.nextInt(numberOfActions));
    }

    public List<Action> getActions() {
        return actions;
    }
}
