package pl.edu.agh.iwium.action;

import robocode.AdvancedRobot;
import robocode.MoveCompleteCondition;

public class Ahead implements Action {

    private static final long serialVersionUID = 1L;

    @Override
    public void execute(AdvancedRobot robot) {
        robot.setAhead(MOVE_DISTANCE);
        robot.execute();
        robot.waitFor(new MoveCompleteCondition(robot));
    }

    @Override
    public int hashCode() {
        return 123;
    }

    @Override
    public boolean equals(Object obj) {
        return getClass() == obj.getClass();
    }
}
