package pl.edu.agh.iwium.action;

import robocode.AdvancedRobot;

public class Fire implements Action {

    private static final long serialVersionUID = 1L;

    @Override
    public void execute(AdvancedRobot robot) {
        robot.setFire(FIRE_POWER);
        robot.execute();
    }

    @Override
    public int hashCode() {
        return 345;
    }

    @Override
    public boolean equals(Object obj) {
        return getClass() == obj.getClass();
    }
}
