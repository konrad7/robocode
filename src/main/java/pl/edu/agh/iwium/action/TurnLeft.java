package pl.edu.agh.iwium.action;

import robocode.AdvancedRobot;
import robocode.TurnCompleteCondition;

public class TurnLeft implements Action {

    private static final long serialVersionUID = 1L;

    @Override
    public void execute(AdvancedRobot robot) {
        robot.setTurnLeft(TURN_DEGREES);
        robot.execute();
        robot.waitFor(new TurnCompleteCondition(robot));
    }

    @Override
    public int hashCode() {
        return 456;
    }

    @Override
    public boolean equals(Object obj) {
        return getClass() == obj.getClass();
    }
}
